import React, { Component } from 'react';
import {Container,Row} from 'reactstrap';
import { getId } from './Handler/Handler';
import Order from './components/Order/Order';
import Products from './components/Ingredients/Ingredients';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            basketItems: [

            ],
            totalPrice: 0
        };
    }


    removeItem(id) {
        const { basketItems } = this.state;

        this.setState({
            basketItems: basketItems.filter(item => item.id !== id)
        });
    }

    addItem(item) {
        const { basketItems } = this.state;

        if (basketItems.some(obj => obj.name === item.name)) {
            this.setState({
                basketItems: basketItems.map(obj => {
                    if (obj.name === item.name) {
                        return {
                            ...obj,
                            count: obj.count + 1,
                            subTotal: obj.price * (obj.count + 1)
                        }
                    }
                    return obj;
                })
            });
        } else {
            this.setState({
                basketItems: [
                    ...basketItems,
                    {
                        ...item,
                        id: getId(),
                        count: 1,
                        subTotal: item.price,
                    }
                ]
            });
        }
    }


    render() {
        const { basketItems } = this.state;

        return (
            <div className="App">
                <h1>Fast Food!!!</h1>
                <Container>
                    <Row>
                        <Order
                            items={basketItems}
                            onRemoveItem={id => this.removeItem(id)}
                        />
                        <Products
                            onAddItem={item => this.addItem(item)}
                        />
                    </Row>
                </Container>
            </div>
        );
    }
}

export default App;
