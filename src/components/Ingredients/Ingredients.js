import React, { Component } from 'react';
import {Col} from 'reactstrap';
import PropTypes from 'prop-types';


const products = [
    {
        ru: 'Бургер',
        price: 80,
        name: 'burger',
        type: 'utensils'
    },
    {
	    ru: 'Кофе',
        price: 20,
	    name: 'coffee',
	    type: 'coffee'
    },
    {
	    ru: 'Шаурма',
        price: 120,
	    name: 'shaurma',
	    type: 'utensils'

    },
    {
	    ru: 'Хот-дог',
        price: 100,
	    name: 'hot-dog',
	    type: 'utensils'
    },
    {
	    ru: 'Coca-Cola',
        price: 40,
	    name: 'cola',
	    type: 'coffee'
    },
    {
	    ru: 'Фри',
        price: 60,
	    name: 'fries',
	    type: 'utensils'
    }
];


class Products extends Component {

    addButtonClick(e, index) {
        e.preventDefault();
        this.props.onAddItem(products[index]);
    }

    render() {
        return (
            <Col sm="8" className="Products content">
                <h2>Наименование:</h2>
                <div className="wrapper">
                    <ul className="Product-items">
                        {products.map((item, index) => (
                            <li key={index}>
                                <button onClick={e => this.addButtonClick(e, index)}>
                                    <span className={'icon fas fa-' + item.type + ''} />
                                    <h4 className="title">{item.ru}</h4>
                                    <p className="cost"><span>Цена:</span> {item.price} <i>KGZ</i></p>
                                </button>
                            </li>
                        ))}
                    </ul>
                </div>
            </Col>
        );
    }
}

Products.propTypes = {
    onAddItem: PropTypes.func.isRequired
};

export default Products;