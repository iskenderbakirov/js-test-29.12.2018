import React, { Component } from 'react';
import {Col} from 'reactstrap';
import PropTypes from 'prop-types';

class Order extends Component {

    removeButtonClicked(e, id) {
        e.preventDefault();
        this.props.onRemoveItem(id);
    }

    render() {
        const { items } = this.props;

        return (
            <Col sm="4" className="Order content">
                <h2>Ваш заказ:</h2>
                <div className="wrapper">
                    {items.length > 0
                        ?
                        <ul className="Order-items">
                            {items.map((item, index) => (
                                <li key={index}>
                                    <span className="name">{item.ru}</span>
                                    <span className="count"> &#65794; {item.count}</span>
                                    <span className="price">{item.subTotal} KGZ</span>
                                    <span className="remove"><button onClick={e => this.removeButtonClicked(e, item.id)}><i className="fas fa-trash-alt" /></button></span>
                                </li>
                            ))}
                        </ul>
                        : <div>
                            <h3>Ваша корзина пуста!</h3>
                            <span>Добавьте блюда, для осуществления заказа!</span>
                        </div>
                    }
                </div>
            </Col>
        );
    }
}

Order.propTypes = {
    onRemoveItem: PropTypes.func.isRequired
};


export default Order;
